#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>  /* struct sockaddr_in */
#include <arpa/inet.h>   /* inet_ntoa() */
#include <sys/time.h>    /* gettimeofday() */
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>      /* select(), perror() */
#include <stdlib.h>      /* getopt(), atoi() */
#include <math.h>        /* fmod() */
#include <fcntl.h>       /* O_BINARY on Win32 */
#include "rtp-tools.h"


#define BIT_0     (	short)0x007f	/* definition of zero-bit in bit-stream      */
#define BIT_1		(short)0x0081	/* definition of one-bit in bit-stream       */
#define SYNC_WORD	(short)0x6b21	/* definition of frame erasure flag          */
#define RATE_8000	80				/* number of speech bits                     */
#define RATE_0		0				/* 0 bit/s rate                  */
#define RATE_SID_OCTET    16		/* number of bits in Octet Transmission mode */

typedef  short int   Word16   ;

/*----------------------------------------------------------------------------
 * ntoitut convert network packet to itut format
 *----------------------------------------------------------------------------
 */
static void ntoitut(char *input, int no_of_bits, Word16 *bitstream)
{
	Word16 iii, bit, mask_bit;

	for (iii = 0 ; iii < no_of_bits ; iii++) {
		mask_bit = iii % 8;
		if ((iii > 0) && (mask_bit == 0)) {
			input++;
		}
		bit = *input & (1 << (7 - mask_bit));      /* get bit */
		if (bit == 0) {
			*bitstream++ = BIT_0;
		}
		else {
			*bitstream++ = BIT_1;
		}
   }
}

/*
 * Print buffer 'data' of length 'len' bytes in itut format to file 'out'.
 * The 'header' with length 'hlen' is needed.
 * Doesn't support G729D and G729E
 * Return 0 if failed, 1 otherwise.
 */
int itut(FILE *out, char *header, int hlen, char *data, int len,int* oldLen_p,u_int32_t* oldTs_p)
{
	int iii;
	int bits = 0;
	long missing;
	Word16 wordBuffer[RATE_8000+2];
	rtp_hdr_t *r = (rtp_hdr_t *)header;
	unsigned long ts = ntohl(r->ts);
	int oldLen = *oldLen_p;
	unsigned long oldTs = *oldTs_p;
	int ptr = 0;

	if(r->pt!=18) return 0;

	if (oldLen<0) {
		oldLen = len;
		oldTs = ts;
	}

	// Count packets lost or silent
	if (ts >= oldTs) {
		missing = ((ts - oldTs) / 80) - (oldLen / 10);
	}
	else {
		missing = ((ts - (0xFFFFFFFF - oldTs) + 1) / 80) - (oldLen / 10);
	}
	// We don't need to insert empty packet if we have a SID frame
	if ((oldLen % 10) == 2) {
		missing--;
	}

	// Replace non existing or silent packets with empty packet in the output bitstream
	for (iii = 0 ; iii < missing ; iii++) {
		wordBuffer[0] = SYNC_WORD;			// sync
		wordBuffer[1] = RATE_0;				// length = 0
		fwrite(wordBuffer, sizeof(Word16), RATE_0 + 2, out);
	}

	(*oldLen_p) = len;
	(*oldTs_p) = ts;

	// Speech packets
	while (len >= 10) {
		wordBuffer[0] = SYNC_WORD;			// sync
		wordBuffer[1] = RATE_8000;			// length
		ntoitut(&data[ptr], RATE_8000, &wordBuffer[2]);
		fwrite(wordBuffer, sizeof(Word16), RATE_8000 + 2, out);

		ptr += 10;
		len -= 10;
	}

	// SID frames
	while (len >= 2) {
		wordBuffer[0] = SYNC_WORD;			// sync
		wordBuffer[1] = RATE_SID_OCTET;		// length
		ntoitut(&data[ptr], RATE_SID_OCTET, &wordBuffer[2]);
		fwrite(wordBuffer, sizeof(Word16), RATE_SID_OCTET + 2, out);

		ptr += 2;
		len -= 2;
	}

	return 1;
} /* itut */
/*
    case F_itut:
      if (ctrl == 0) {
        hlen = parse_header(packet->p.data);
        itut(out, packet->p.data, hlen, packet->p.data + hlen, trunc < len ? trunc : len - hlen);
      }
      break;
*/
